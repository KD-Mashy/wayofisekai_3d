        WAY OF ISEKAI 3D
    
    Genre: RPG, Anime - Isekai, TPS

Az előző reponál elhibáztam egy elég nagynak számító dolgot és használhatatlanná vált számomra, így újra kellett csinálnom hogy tudjak vele dolgozni.

        RÖVID BEMUTATÁSA:

A Way Of Isekai cím a játék története alapján ihletődött, mondhatni az animék Isekai stílusát követi a játék. Az Isekai jelentése saját szavakkal, röviden hogy egy másik világba teleportálva játszódó történet, sok olyan anime van aminek a címében is benne van,.pl: Isekai Cheat Magician.

Az ilyen történethez illően, valami különlegessel kerül a „játékos” a másik világba, ahol különböző területeken, különböző küldetéseket kell teljesítenie, hogy visszajusson a saját világába. Viszont nem csak teleportálással történhet egy ilyen világba kerülés…

2 Helyen írom jelenleg a játékot, mivel:
    
    1.	A Programozás Módszertan I órára szintén ezt alkotom, fejlesztem tovább a korábbi állapotáról Java nyelven. 
    Itt tervezem meg a dolgokat és fejlesztem tovább azt a programot, melyet az előző félévben beadandónak készítettem ElemiProg. órára RPGJáték néven.
    
    2.	3D-be tervezem átvezetni az alapjait, és megalkotni ténylegesen a játékot, Unity használatával.

        TARTALMA:

5 Területre van osztva a küldetések rész, mindegyiknek megvan a sajátossága, mint a kinézete, és az ellenfelek (azoknak az erőssége, fajtája is különbözik).

A különböző területeket, különböző szintenként lehet elérni , pl: 15-30-45-60 volt, de változni fog valószínűleg, illetve minden terület farmolható, és az alap story rész lejátszása után (lényegében az 1. clear után) alternatív dolgok is megjelenhetnek, tervben vannak.

Az egyes területekre tervben vannak NPC-k is akár mellék küldetésekhez, és egyebekhez is.

Szintrendszert vezettem be a játékban, ami alapján scalel az enemy is, illetve nincs LvL cap, szóval akármeddig lehet fejlődni.
Tervben van az itemek bevezetése, nem valószínű hogy megvalósul, de hajlok felé.

        TECH:

A tervek szerint kell lennie:
    •	Harcrendszer, HUD hozzá
    •	Útkeresés módszer (?csak visszanézni tudom az órát, így jelen pillanatban még kérdés számomra?) az ellenfeleknek
    •	Szintrendszer bevezetése
    •	Amint lehet saját textúrák, illetve 3d modellek
    •	Inventory létrehozása (kétséges még)

        JÁTÉKBÓL (VALÓSZÍNŰLEG ANGOLUL, AZ EREDETI PROGRAMOM ALAPJÁN):

        QUESTEK[1-5]:

1. - Unexpected Journey

2. - To The Mountains

3. - Caves of the Mountain

4. - Border of Wilderness

5. - Dome of DOOM


        MINIGAMEK (TERVBEN VAN TÖBB):

Sheldon (Kő papír olló gyík Spoch)

        ELLENFELEK (BY QUEST [1-5]):

1. - Wolf, Bear, Vyvern

2. - Goblin, Harpy worker, Harpy Warrior

3. - Troll, High Mount Goblin, Goblin King

4. - Watcher, Behemoth, Thunderjaw

5. - Werewolf, Vampire, ????????


        SPOILER:

A játék tartalmazhat egyes részleteket, melyek más játékokból származnak, talán anime, vagy sorozatból származhatnak, illetve valós tényezők is lehetnek benne, mint easter egg-ek.

Megjegyezném hogy az egyes tényezők tulajdon joga pedig nem az enyém, főleg mivel nem egy hivatalos játékról van szó, mely értékesítésre lesz szánva, hanem saját, illetve a beadandó munkám képezi csak.

UI: ha esetleg meglenne változtatva ezen részletek többsége, vagy mind, akkor nem ellenkezek az emberek elé tárása ellen.
    
        By: Klepe Dominik, KDMashy, kagethk@gmail.com
        
        Bug fixelésem Unity/Gitben: https://youtu.be/8eUcHobLE4U?t=666

    UPDATE:

05.11 13:57 Kész a Demo.
